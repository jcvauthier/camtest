#!/bin/bash
#$1 is the username  and $2 is the user password 
useradd -p $(perl -e "print crypt(\"$2\", \"wtf\")") $1
passwd -u $1
usermod -aG wheel $1