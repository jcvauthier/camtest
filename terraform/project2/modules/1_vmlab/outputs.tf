output "vmlab_id" {
  value = openstack_compute_instance_v2.vmlab.id
}

output "vmlab_ip" {
  depends_on = [
    null_resource.vmlab_init]
  value = openstack_compute_instance_v2.vmlab.access_ip_v4
}