data "openstack_compute_flavor_v2" "vmlab" {
  name = var.vmlab["instance_type"]
}

resource "openstack_compute_keypair_v2" "key-pair" {
  count = var.create_keypair
  name = var.keypair_name
  public_key = var.public_key
}

resource "random_id" "label" {
  count = var.scg_id == "" ? 0 : 1
  byte_length = "2"
}

resource "openstack_compute_flavor_v2" "vmlab_scg" {
  count = var.scg_id == "" ? 0 : 1
  name = "${var.vmlab["instance_type"]}-${random_id.label[0].hex}-scg"
  region = data.openstack_compute_flavor_v2.vmlab.region
  ram = data.openstack_compute_flavor_v2.vmlab.ram
  vcpus = data.openstack_compute_flavor_v2.vmlab.vcpus
  disk = data.openstack_compute_flavor_v2.vmlab.disk
  swap = data.openstack_compute_flavor_v2.vmlab.swap
  rx_tx_factor = data.openstack_compute_flavor_v2.vmlab.rx_tx_factor
  is_public = data.openstack_compute_flavor_v2.vmlab.is_public
  extra_specs = merge(data.openstack_compute_flavor_v2.vmlab.extra_specs, {
    "powervm:storage_connectivity_group": var.scg_id
  })
}

resource "openstack_compute_instance_v2" "vmlab" {
  name = "${var.vmlab_id}-vm"
  image_id = var.vmlab["image_id"]
  flavor_id = var.scg_id == "" ? data.openstack_compute_flavor_v2.vmlab.id : openstack_compute_flavor_v2.vmlab_scg[0].id
  key_pair = openstack_compute_keypair_v2.key-pair.0.name
  network {
    name = var.network_name
  }
  availability_zone = var.openstack_availability_zone
}

# To check vmlab availability before running commands (provisioners) 
resource "null_resource" "check_vmlab" {

  #Connect to the vm as root in order to run the script with right privilege
  connection {
    type        = "ssh"
    host        = openstack_compute_instance_v2.vmlab.access_ip_v4
    user        = var.vmlab_username
    password    = var.vmlab_userpassword
    timeout     = "15m"
  } 
 
  provisioner "remote-exec" {
      inline = [
        "whoami"
      ]
  }
 

  # For the vmlab to authorize the desktop user to do an ssh connection
  provisioner "file" {
    content = var.private_key
    destination = "~/.ssh/id_rsa"
  }
 
  provisioner "file" {
    content = var.public_key
    destination = "~/.ssh/id_rsa.pub"
  }

  # Add a user with password 
  provisioner "file" {
    #copy the script from the local script module to the remote /tm folder 
    source      = "${path.module}/scripts/adduser.sh"
    destination = "/tmp/adduser.sh"
  }

  provisioner "remote-exec" {
    #Execute the adduser script on the remote host, then remove the script 
    inline = [
      "chmod 600 ~/.ssh/id_rsa",   
      "chmod +x /tmp/adduser.sh",
      "/tmp/adduser.sh ${var.vmlab_studentname}  ${var.vmlab_studentpassword} && rm /tmp/adduser.sh",  
      "exit"
    ]
  }
}