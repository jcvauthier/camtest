variable "vmlab_id" {
  default = ""
}
variable "vmlab" {
  default = {
    instance_type = ""
    image_id = ""
  }
}

variable "network_name" {}
variable "scg_id" {}
variable "openstack_availability_zone" {}
variable "vmlab_username" {}
variable "vmlab_userpassword" {}
variable "vmlab_studentname" {}
variable "vmlab_studentpassword" {}

#openstack connexion
variable "private_key" {}
variable "public_key" {}
variable "create_keypair" {}
variable "keypair_name" {}