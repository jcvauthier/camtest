terraform {
  required_version = ">= 0.12.0, < 0.13.0"
  required_providers {
    openstack = {
      version = "1.29.0"
    }
    random = { 
      version = "2.3.0"
    }   
  } 
}

resource "random_id" "label" {
  byte_length = "2"
  # Since we use the hex, the word lenght would double
  prefix = "${var.vmlab_id_prefix}-"
}

module "vmlab" {
  source = "./modules/1_vmlab"
  vmlab_id = "${random_id.label.hex}"
  vmlab = var.vmlab
  network_name = var.network_name
  scg_id = var.scg_id
  openstack_availability_zone = var.openstack_availability_zone
  vmlab_username = var.vmlab_username
  vmlab_userpassword = var.vmlab_userpassword
  vmlab_studentname = var.vmlab_studentname
  vmlab_studentpassword = var.vmlab_studentpassword
  private_key = local.private_key
  public_key = local.public_key
  create_keypair = local.create_keypair
  keypair_name = "${random_id.label.hex}-keypair"
}