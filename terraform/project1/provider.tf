provider "openstack" {
  version = "~> 1.29.0"
  user_name = var.user_name
  password = var.password
  tenant_name = var.tenant_name
  domain_name = var.domain_name
  auth_url = var.auth_url
  insecure = var.insecure
}

provider "random" {
  version = "~> 2.3.0"
} 


