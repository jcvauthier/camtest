variable "vmlab_id" {
  default = ""
}
variable "vmlab" {
  default = {
    instance_type = ""
    image_id = ""
  }
}

variable "network_name" {}
variable "scg_id" {}
variable "openstack_availability_zone" {}
variable "vmlab_username" {}

#openstack connexion
variable "private_key" {}
variable "public_key" {}
variable "create_keypair" {}
variable "keypair_name" {}

