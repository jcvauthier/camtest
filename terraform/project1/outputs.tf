output "vmlab_id" {
  value = module.vmlab.vmlab_id
}

output "vmlab_ip" {
  value = module.vmlab.vmlab_ip
}
